import React, { useEffect, useState } from "react";
import instagramLogo from "../assets/owner/instagram.png";
import moreIcon from "../assets/owner/more.png";
import twitterLogo from "../assets/owner/twitter.png";
import "./Main.css";

const Main = ({ selectedPunk, punkListData }) => {
  const [activePunk, setActivePunk] = useState(punkListData[0]);

  useEffect(() => {
    setActivePunk(punkListData[selectedPunk]);
  }, [punkListData, selectedPunk]);

  return (
    <div className="main">
      <div className="main-content">
        <div className="punk-highlight">
          <div className="punk-container">
            <img
              className="selected-punk"
              src={activePunk.image_original_url}
              alt=""
            />
          </div>
        </div>

        <div className="punk-details">
          <div className="punk-title">
            <div className="title">{activePunk.name}</div>
            <span className="item-number">.#{activePunk.token_id}</span>
          </div>

          <div className="owner">
            <div className="owner-image-container">
              <img src={activePunk.owner.profile_img_url} alt="" />
            </div>
            <div className="owner-details">
              <div className="owner-name-and-handle">
                <p>{activePunk.owner.address}</p>
                <p className="owner-handle">@{activePunk.owner.user.username}</p>
              </div>
              <div className="owner-link">
                <img src={instagramLogo} alt="" />
              </div>
              <div className="owner-link">
                <img src={twitterLogo} alt="" />
              </div>
              <div className="owner-link">
                <img src={moreIcon} alt="" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Main;
