import weth from '../assets/weth.png'
import './CollectionCard.css'

const CollectionCard = ({id,image,name,traits}) => {
    return ( <div className="collection-card">
        <img src={image} alt="" />
        <div className="details">
        <div className="name">{name} <p className="id">.#{id}</p> </div>
        <div className="price-container">
            <img src={weth} className="weth-image"  alt="" />
            <p>{traits[0].value}</p>
        </div>
        </div>
    </div> );
}
 
export default CollectionCard;